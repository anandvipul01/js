console.log('Hello World');

/*****************
 * Variables
 * Number, String, Boolean, Undefined, Null
*/
//  var firstName = 'John';
// var lastName = 'Smith';
// var age = 28;

// var fullAge = true;
// // var job = 'Teacher';
// var job;
// var _vipul = 'Vipul';
// var $vipul = 'Vipul';

// console.log(firstName)
// console.log(fullAge)
// console.log(job)
// console.log(_vipul)
// console.log($vipul)

/******************
 * Variables Mutation and Type Coercion
 */

 // Type Coercion
// var firstName = 'John';
// var age = 28;

// console.log(firstName + ' ' + age); // Type Coercion


// var job, isMarried;
// job = 'Teacher';
// isMarried = false;

// console.log(firstName + ' is ' + age + ' year old ' + ' he is a ' + job + '. Is he Married ' + isMarried + '!');

// // Variable Mutation

// age = 'twenty eight';

// alert(firstName + ' is ' + age + ' year old ' + ' he is a ' + 
// job + '. Is he Married ' + isMarried + '!');




// // Getting data from the user

// var lastName = prompt('What is his last name? ');
// console.log(firstName + ' ' + lastName);





/******************
 * Basic Operators
 */
// var year, yearJohn, yearMark;
// var now = 2018;
// var ageJohn = 28;
// var ageMark = 33;
// var yearJohn = now - ageJohn;
// var yearMark = now - ageMark;
// console.log(yearJohn);
// console.log(yearMark);

// console.log(now + 2);
// console.log(now * 2);
// console.log(now / 10);

// var johnOlder = ageJohn > ageMark;
// console.log(johnOlder);

// var johnOlder = ageJohn < ageMark;
// console.log(johnOlder);

// var johnOlder = ageJohn == ageMark;
// console.log(johnOlder);

// // typeof operator
// console.log(typeof johnOlder);
// console.log(typeof ageJohn);
// console.log(typeof "ddd");
// var y;
// console.log(typeof y);


/******************
 * Operator Precedence
 */

// var now = 2018;
// var yearJohn = 1989;
// var fullAge = 18;

// var isFullAge = now - yearJohn >= fullAge;
// console.log(isFullAge);
// var ageJohn = now - yearJohn;
// var ageMark = 35;
// var average = (ageJohn + ageMark) / 2;
// console.log(average);

// var x, y;
// x = y = (3 + 5) + 4 -6;
// console.log(x,y);

// x *= 2;
// x++;
// console.log(x);

/******************
 * If ELse Statements
 */
// var firstName = 'John';
// var civilStatus = 'single';
// if (civilStatus === 'married') {
//     console.log(firstName + " is married! ");
// } else {
//     console.log(firstName + ' will hopefully marry soon!');
// }

// var isMarried = false;

// if (isMarried) {
//     console.log(firstName + " is married! ");
// } else {
//     console.log(firstName + ' will hopefully marry soon!');
// }

/******************
 * Boolean Logic
 */

// var firstName = 'John';
// var age = 44;

// if (age < 13) {
//     console.log(firstName + ' is a boy.');
// } else if (age < 20 && age >= 13){ //Between 13 and 20
//     console.log('He is a teenager');
// } else if (age > 20 && age <30 ){
//     console.log(firstName + ' is a young man.');
// } else {
//     console.log('he is a Man!');
// }


/******************
 * The ternary Operator and Switch Statements
 */
// Ternary Operator
//  var firstName = 'John';
//  var age = 16;

//  age >= 18 ? 
//  console.log(firstName + ' drinks Beer!')
//  : console.log(firstName + ' drinks juice');


// var drink = age >= 18 ? 'beer' : 'juice';
// console.log(drink);

// // Switch statements

// var job = 'teaceher';
// switch (job) {
//     case 'teacher':
//     case 'instructor':
//         console.log('Teacher Hitted');
//         break;
//     case 'driver':
//         console.log('Driver Hit');
//         break;
//     default:
//         console.log('None Matched');
// }

// switch (true) {
//     case age < 13:
//         console.log('Age is 13');
//         break;
//     case age >= 13 && age < 20:
//         console.log('Teen');
//         break;
//     default:
//         console.log('Not a teen anymore');
// }


/******************
 * Truthly and Falsly values and equality operators
 */
// Truthly values : Not falsy values
// falsy values : undefined, null, 0, '', NaN
//  var height;
// height = 23;
//  if (height || height === 0) {
//      console.log('Variable is defined.');
//  } else {
//      console.log('Varibale has not been defined');
//  }

//  // Equality Operator

//  if (height == '23') {
//      console.log('The == Operator does type coercion')
//  }

 /******************
 * Functions
 */

//  function calculateAge(birthYear) {
//     return 2018 - birthYear;
//  }

//  var ageJohn = calculateAge(1994);
//  var ageMike = calculateAge(1990);
//  var ageJane = calculateAge(1998)
//  console.log(ageJohn, ageJane, ageMike);


//  function yearUntilRetirement(year, firstName) {
//      var age = calculateAge(year);
//      var retirement = 65 - age;
//      if (retirement > 0) {
//         console.log(firstName + ' retires in ' + retirement);
//      } else {
//         console.log(firstName + ' Already Retired ');

//      }
     
//  }

//  yearUntilRetirement(1920, 'Vipul');

/******************
 * Functions Statements and Expresssions
 */

//  var whatDoYouDo = function(job, firstName) {
//      switch(job) {
//          case 'teacher':
//              return firstName + ' teaches';

//          case 'driver':
//              return firstName + ' drives';

//          case 'designer':
//              return firstName + ' designes';

//          default:
//              return firstName + ' looking for an oppurtunity';

//      }
//  }

//  console.log(whatDoYouDo('teacher', 'John'))

/******************
 * Arrays
 */

//  var names = ['John', 'Mark', 'Jane'];
//  var years = new Array(1990, 1969, 1948);
//  console.log(names[0]);
//  console.log(names.length);

//  names[1] = 'Ben';
//  console.log(names);

//  names[names.length] = 'Mary';
//  console.log(names);

//  // Different Data Types

//  var john = ['John', 'Smith', 1990,  var  'teacher', 'favar johlse'];
//  john.push('blue');
//  console.log(john);
//  john.unshift('Mr.');
//  john.pop();
//  john.shift();
//  console.log(john);
//  console.log(john.indexOf(1990));
//  console.log(john.indexOf(1992));

 /******************
 * Objects and Properties
 */

// var john = {
//     firstName: 'John',
//     lastName: 'Smith',
//     birthYear: 1990,
//     family: ['Jane', 'Mark', 'Bob', 'Emily'],
//     jobs: 'teacher',
//     isMarried: false
// };
// console.log(john);
// console.log(john.firstName);
// var x = 'birthYear';
// console.log(john['lastName']);
// console.log(john[x]);

// john.job = 'designer';
// john['birthYear'] = 1994;
// console.log(john);

// var jane = new Object();
// jane.firstName = 'Jane';
// jane['lastName'] = 'Smith';
// jane.birthYear = 1990
// console.log(jane);

 /******************
 * Objects and Methods
 */

//  var john = {
//     firstName: 'John',
//     lastName: 'Smith',
//     birthYear: 1990,
//     family: ['Jane', 'Mark', 'Bob', 'Emily'],
//     jobs: 'teacher',
//     isMarried: false,
//     calcAge: function() {
//         this.age = 2018 - this.birthYear;
//         return 2018 - this.birthYear;
//     }
// };
// john.calcAge();

// console.log(john);

 /******************
 * Loops and Iterators
 */

//  for (var i = 0; i < 10; i++) {
//      console.log(i);
//  }

//  for (var i = 0; i < 10; i += 2) {
//     console.log(i);
// }

// var john = ['John', 'Smith', 1990, 'teacher', 'favar johlse'];

// for (var i = 0; i < john.length; i++) {
//     console.log(john[i]);
// }

// var i = 0;
// while (i < john.length) {
//     console.log(john[i]);
//     i++;
// }



// var john = ['John', 'Smith', 1990, 'teacher', 'favar johlse'];

// for (var i = 0; i < john.length; i++) {
//     if (typeof john[i] !== 'string') {
//         continue;
//     }
//     console.log(john[i]);
// }

// var john = ['John', 'Smith', 1990, 'teacher', 'favar johlse'];

// for (var i = 0; i < john.length; i++) {
//     if (typeof john[i] !== 'string') {
//         break;
//     }
//     console.log(john[i]);
// }

// console.log('-------------Challenge-------------');
// for (var i = john.length; i >= 1; i--) {
//     if (typeof john[i-1] !== 'string') {
//         break;
//     }
//     console.log(john[i-1]);
// }

 /******************
 * Challenges
 */
// 5. Tip Calculater 2 

var tipCaluculator2 = {
    bill_array: [124, 48, 268, 180, 42],
    // bill_tip: [],
    // total_cost: [],
    calTip: function(bill) {
        if (bill > 0 && bill < 50) {
            return bill*0.2;
        } else if (bill < 200 && bill > 50) {
            return bill*0.15;
        } else if (bill > 200){
            return bill*0.1;
        } else {
            return 0;
        }
    },
    bill_tip: function() {
        var bill_tip = [];
        var total_cost = [];
        for (var i = 0; i < this.bill_array.length; i++) {
            bill_tip.push(this.calTip(this.bill_array[i]));
            total_cost.push(this.bill_array[i] + this.calTip(this.bill_array[i]));
        }
        return {'Bill Tip Amount': bill_tip, 'Total amount to pay': total_cost};
    }

};
var b = tipCaluculator2.bill_tip();
console.log('------------Challenge: 5---------------');
console.log(b);
console.log('--------------------END----------------');



 /******************
 * Challenges
 */
// 4. BMI - 2
var john_bmi = {
    fullName: 'John Smith',
    mass: 68,
    height: 1.5,
    bmiCal: function() {
        res = this.mass/(this.height*this.height);
        this.bmi= res;
        // return this.mass/(this.height*this.height);
    },
}


var mark_bmi = {
    fullName: 'John Smith',
    mass: 98,
    height: 1.5,
    bmiCal: function() {
        res = this.mass/(this.height*this.height);
        this.bmi= res;
        // return this.mass/(this.height*this.height);
    },
}

console.log('------------Challenge: 4---------------');



john_bmi.bmiCal();
mark_bmi.bmiCal();
console.log('BMI of John ' + john_bmi.bmi);
console.log('BMI of Mark ' + mark_bmi.bmi);

if (mark_bmi.bmi > john_bmi.bmi) {
    console.log('Mark Has higher BMI');
} else {
    console.log('John Has higher BMI');
}

console.log('--------------------END----------------');

